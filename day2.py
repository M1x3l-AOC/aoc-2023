from inputs import day_2 as input
from math import prod


def parse_line(line: str):
    [game, record] = line.split(":")
    id = int(game[5:])
    subsets = [
        [infos.strip().split(" ") for infos in s.split(",")] for s in record.split(";")
    ]
    return id, subsets


def get_game_count(subsets: "list[list[list[str,str]]]"):
    counts = {"red": 0, "green": 0, "blue": 0}
    for set in subsets:
        for info in set:
            if (c := int(info[0])) > counts[info[1]]:
                counts[info[1]] = c

    return counts


def part_1(input: str):
    id_sum = 0
    for line in input.splitlines():
        id, subsets = parse_line(line)
        counts = get_game_count(subsets)

        if counts["red"] <= 12 and counts["green"] <= 13 and counts["blue"] <= 14:
            id_sum += id

    return id_sum


def part_2(input: str):
    power_sum = 0
    for line in input.splitlines():
        id, subsets = parse_line(line)
        counts = get_game_count(subsets)
        power_sum += prod(counts.values())

    return power_sum


print("Part 1:", part_1(input))
print("Part 2:", part_2(input))
