from inputs import day_8 as input
from math import gcd
from functools import reduce


def lcm(arr):
    return reduce(lambda x, y: (x * y) // gcd(x, y), arr)


def get_node_map(nodes: str):
    return dict(
        [
            (node[0], (node[1][1:4], node[1][6:9]))
            for node in [node.split(" = ") for node in nodes.splitlines()]
        ]
    )


def part_1(input: str):
    [instructions, nodes] = input.split("\n\n")
    node_map = get_node_map(nodes)

    current_node = "AAA"
    steps = 0
    while current_node != "ZZZ":
        instruction = instructions[steps % len(instructions)]
        current_node = node_map[current_node][0 if instruction == "L" else 1]
        steps += 1

    return steps


def part_2(input: str):
    [instructions, nodes] = input.split("\n\n")
    node_map = get_node_map(nodes)

    current_nodes = [x for x in node_map if x[2] == "A"]

    steps = 0

    occurrences = [[] for _ in current_nodes]

    while not all([node[2] == "Z" for node in current_nodes]):
        instruction = instructions[steps % len(instructions)]
        for i, node in enumerate(current_nodes):
            current_nodes[i] = node_map[node][0 if instruction == "L" else 1]
            if node[2] == "Z":
                occurrences[i].append(steps)
        if all(occurrences):
            return lcm([o[0] for o in occurrences])
        steps += 1


print("Part 1:", part_1(input))
print("Part 2:", part_2(input))
