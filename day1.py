from inputs import day_1 as input
from math import inf

lookup = {
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9,
}


def find_first_number_p1(s: str):
    for c in list(s):
        if c.isnumeric():
            return c


def find_last_number_p1(s: str):
    rev_s = list(s)
    rev_s.reverse()
    for c in rev_s:
        if c.isnumeric():
            return c


def find_first_number_p2(s: str):
    indexes_num = [c if (c := s.find(str(num))) >= 0 else inf for num in range(10)]
    indexes_word = [
        (num, c) if (c := s.find(str(num))) >= 0 else (num, inf)
        for num in lookup.keys()
    ]

    if min(indexes_num) < min([x[1] for x in indexes_word]):
        return s[min(indexes_num)]
    else:
        minimum = inf
        word = ""
        for w in indexes_word:
            if w[1] < minimum:
                minimum = w[1]
                word = w[0]

        return lookup[s[minimum : minimum + len(word)]]


def find_last_number_p2(s: str):
    for i in range(len(s) + 1):
        indexes_num = [
            c if (c := s.find(str(num), len(s) - i)) >= 0 else -inf for num in range(10)
        ]
        indexes_word = [
            (num, c) if (c := s.find(str(num), len(s) - i)) >= 0 else (num, -inf)
            for num in lookup.keys()
        ]

        if (
            not max(indexes_num) == -inf
            or not max([x[1] for x in indexes_word]) == -inf
        ):
            if max(indexes_num) > max([x[1] for x in indexes_word]):
                return s[max(indexes_num)]
            else:
                maximum = -inf
                word = ""
                for w in indexes_word:
                    if w[1] > maximum:
                        maximum = w[1]
                        word = w[0]
                return lookup[s[maximum : maximum + len(word)]]


def part_1(input):
    sum = 0
    for line in input.splitlines():
        sum += int(find_first_number_p1(line)) * 10 + int(find_last_number_p1(line))

    return sum


def part_2(input):
    sum = 0
    for line in input.splitlines():
        sum += int(find_first_number_p2(line)) * 10 + int(find_last_number_p2(line))

    return sum


print("Part 1:", part_1(input))
print("Part 2:", part_2(input))
