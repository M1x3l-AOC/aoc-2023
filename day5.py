from inputs import day_5 as input
from math import inf
import time


def compile_input(input: str):
    seeds, *sections = input.split("\n\n")
    seeds = [int(n) for n in seeds.split(":")[1].strip().split(" ")]
    return seeds, sections


def generate_lookup(source_text: str):
    ranges = []
    for line in source_text.splitlines()[1:]:
        ranges.append([int(n) for n in line.split(" ")])

    def lookup(n: int):
        for range in ranges:
            if range[1] <= n <= range[1] + range[2]:
                return range[0] + (n - range[1])
        return n

    return lookup


def seed_to_location(lookups: "list[function]", seed: int):
    # lookups[0] seed_to_soil,
    # lookups[1] soil_to_fertilizer,
    # lookups[2] fertilizer_to_water,
    # lookups[3] water_to_light,
    # lookups[4] light_to_temperature,
    # lookups[5] temperature_to_humidity,
    # lookups[6] humidity_to_location,
    return lookups[6](
        lookups[5](lookups[4](lookups[3](lookups[2](lookups[1](lookups[0](seed))))))
    )


def part_1(input: str):
    seeds, sections = compile_input(input)
    lookups = [generate_lookup(section) for section in sections]

    return min([seed_to_location(lookups, s) for s in seeds])


def part_2(input: str):
    seeds, sections = compile_input(input)
    range_starts, range_lengths = seeds[::2], seeds[1::2]
    lookups = [generate_lookup(section) for section in sections]

    minimum = inf
    for range_start, range_length in zip(range_starts, range_lengths):
        for s in range(range_start, range_start + range_length):
            if (loc := seed_to_location(lookups, s)) < minimum:
                minimum = loc
    return minimum


print("Part 1:", part_1(input))
print("Part 2:", part_2(input))
