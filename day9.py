from inputs import day_9 as input


def parse_sequences(input: str):
    return [[int(n) for n in l.split(" ")] for l in input.splitlines()]


def sequence_diff(seq: list):
    return [sum(p) for p in zip([-n for n in seq[:-1]], seq[1:])]


def extrapolate(seqs: list, part):
    target = 0
    for s in seqs[-2::-1]:
        if part == 2:
            target = s[0] - target
        else:
            target += s[-1]

    return target


def part(input: str, part=1):
    extrapolations = []
    for s in parse_sequences(input):
        subsequences = [s]
        while any(subsequences[-1]):
            subsequences.append(sequence_diff(subsequences[-1]))
        extrapolations.append(extrapolate(subsequences, part))
    return sum(extrapolations)


print("Part 1:", part(input, 1))
print("Part 2:", part(input, 2))
