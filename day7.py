from inputs import day_7 as input


def get_type(cards: str, part=1):
    """
    `6` => 5 of a kind \\
    `5` => 4 of a kind \\
    `4` => Full House \\
    `3` => 3 of a kind \\
    `2` => 2 pair \\
    `1` => 1 pair \\
    `0` => high card
    """

    s = {
        "2": 0,
        "3": 0,
        "4": 0,
        "5": 0,
        "6": 0,
        "7": 0,
        "8": 0,
        "9": 0,
        "T": 0,
        "J": 0,
        "Q": 0,
        "K": 0,
        "A": 0,
    }

    for c in cards:
        s[c] += 1

    if part == 2:
        jokers = s.pop("J")

    values = list(s.values())
    if part == 1 or jokers == 0:
        if 5 in values:
            return 6
        elif 4 in values:
            return 5
        elif 3 in values and 2 in values:
            return 4
        elif 3 in values:
            return 3
        elif 2 in values:
            values.remove(2)
            if 2 in values:
                return 2
            else:
                return 1
        else:
            return 0
    else:
        if 5 in values:
            return 6
        elif 4 in values:
            return 6
        elif 3 in values:
            if jokers == 2:
                return 6
            else:
                return 5
        elif 2 in values:
            values.remove(2)
            if jokers == 3:
                return 6
            elif jokers == 2:
                return 5
            else:
                if 2 in values:
                    return 4
                else:
                    return 3
        else:
            if jokers == 5 or jokers == 4:
                return 6
            elif jokers == 3:
                return 5
            elif jokers == 2:
                return 3
            return 1


maps = [
    {
        "2": 0x0,
        "3": 0x1,
        "4": 0x2,
        "5": 0x3,
        "6": 0x4,
        "7": 0x5,
        "8": 0x6,
        "9": 0x7,
        "T": 0x8,
        "J": 0x9,
        "Q": 0xA,
        "K": 0xB,
        "A": 0xC,
    },
    {
        "J": 0x0,
        "2": 0x1,
        "3": 0x2,
        "4": 0x3,
        "5": 0x4,
        "6": 0x5,
        "7": 0x6,
        "8": 0x7,
        "9": 0x8,
        "T": 0x9,
        "Q": 0xA,
        "K": 0xB,
        "A": 0xC,
    },
]


def get_value(cards: str, part=1):
    value = 0
    for i, c in enumerate(cards):
        value += maps[part - 1][c] * 0x10 ** (5 - i - 1)
    return value


def part(input: str, part):
    games = []
    for line in input.splitlines():
        [cards, bid] = line.split(" ")
        games.append(
            {
                "value": get_type(cards, part) * 0x10**5 + get_value(cards, part),
                "bid": int(bid),
            }
        )
        games = sorted(games, key=lambda g: g["value"])

    return sum([i * g["bid"] for i, g in enumerate(games, 1)])


print("Part 1:", part(input, 1))
print("Part 2:", part(input, 2))
