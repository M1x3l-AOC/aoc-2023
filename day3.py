from inputs import day_3 as input


def id_fn_gen():
    counter = 0
    while True:
        yield counter
        counter += 1


def generate_number_cells(field):
    number_cells = [
        [{"value": "0", "id": 0} for _ in range(len(field[0]))]
        for _ in range(len(field))
    ]
    gen_id = id_fn_gen()
    for i, line in enumerate(field):
        for j, char in enumerate(line):
            if char.isnumeric():
                number_cells[i][j]["value"] = char
                if j >= 1 and (c := field[i][j - 1]).isnumeric():
                    number_cells[i][j]["value"] = c + number_cells[i][j]["value"]
                    if j >= 2 and (c := field[i][j - 2]).isnumeric():
                        number_cells[i][j]["value"] = c + number_cells[i][j]["value"]
                if j < len(line) - 1 and (c := field[i][j + 1]).isnumeric():
                    number_cells[i][j]["value"] = number_cells[i][j]["value"] + c
                    if j < len(line) - 2 and (c := field[i][j + 2]).isnumeric():
                        number_cells[i][j]["value"] = number_cells[i][j]["value"] + c
                if (
                    j >= 1
                    and number_cells[i][j - 1]["value"] == number_cells[i][j]["value"]
                ):
                    number_cells[i][j]["id"] = number_cells[i][j - 1]["id"]
                else:
                    number_cells[i][j]["id"] = next(gen_id)
    return number_cells


def part_1(input: str):
    field = input.splitlines()

    number_cells = generate_number_cells(field)
    number_cells = [
        [{"value": int(c["value"]), "id": c["id"]} for c in l] for l in number_cells
    ]

    checked_ids = []
    num_sum = 0
    for i, line in enumerate(field):
        for j, char in enumerate(line):
            if not (char.isnumeric() or char == "."):
                if i >= 1:
                    if (
                        j >= 1
                        and (c := number_cells[i - 1][j - 1])["value"]
                        and not number_cells[i - 1][j - 1]["id"] in checked_ids
                    ):
                        num_sum += c["value"]
                        checked_ids.append(c["id"])
                    if (c := number_cells[i - 1][j])["value"] and not number_cells[
                        i - 1
                    ][j]["id"] in checked_ids:
                        num_sum += c["value"]
                        checked_ids.append(c["id"])
                    if (
                        j < len(line)
                        and (c := number_cells[i - 1][j + 1])["value"]
                        and not number_cells[i - 1][j + 1]["id"] in checked_ids
                    ):
                        num_sum += c["value"]
                        checked_ids.append(c["id"])
                if i < len(field):
                    if (
                        j >= 1
                        and (c := number_cells[i + 1][j - 1])["value"]
                        and not number_cells[i + 1][j - 1]["id"] in checked_ids
                    ):
                        num_sum += c["value"]
                        checked_ids.append(c["id"])
                    if (c := number_cells[i + 1][j])["value"] and not number_cells[
                        i + 1
                    ][j]["id"] in checked_ids:
                        num_sum += c["value"]
                        checked_ids.append(c["id"])
                    if (
                        j < len(line)
                        and (c := number_cells[i + 1][j + 1])["value"]
                        and not number_cells[i + 1][j + 1]["id"] in checked_ids
                    ):
                        num_sum += c["value"]
                        checked_ids.append(c["id"])
                if (
                    j >= 1
                    and (c := number_cells[i][j - 1])["value"]
                    and not number_cells[i][j - 1]["id"] in checked_ids
                ):
                    num_sum += c["value"]
                    checked_ids.append(c["id"])
                if (
                    j < len(line)
                    and (c := number_cells[i][j + 1])["value"]
                    and not number_cells[i][j + 1]["id"] in checked_ids
                ):
                    num_sum += c["value"]
                    checked_ids.append(c["id"])

    # print(checked_ids)
    # print(
    #     "\n".join(
    #         [
    #             " ".join(l)
    #             for l in [
    #                 [
    #                     f"{str(cell['value']):>3} [{str(cell['id']):>4}] |"
    #                     for cell in line
    #                 ]
    #                 for line in number_cells
    #             ]
    #         ]
    #     )
    # )
    return num_sum


def part_2(input: str):
    field = input.splitlines()

    number_cells = generate_number_cells(field)
    number_cells = [
        [{"value": int(c["value"]), "id": c["id"]} for c in l] for l in number_cells
    ]

    checked_ids = []
    gear_sum = 0
    for i, line in enumerate(field):
        for j, char in enumerate(line):
            if char == "*":
                values = []
                if i >= 1:
                    if (
                        j >= 1
                        and (c := number_cells[i - 1][j - 1])["value"]
                        and not number_cells[i - 1][j - 1]["id"] in checked_ids
                    ):
                        values.append(c["value"])
                        checked_ids.append(c["id"])
                    if (c := number_cells[i - 1][j])["value"] and not number_cells[
                        i - 1
                    ][j]["id"] in checked_ids:
                        values.append(c["value"])
                        checked_ids.append(c["id"])
                    if (
                        j < len(line)
                        and (c := number_cells[i - 1][j + 1])["value"]
                        and not number_cells[i - 1][j + 1]["id"] in checked_ids
                    ):
                        values.append(c["value"])
                        checked_ids.append(c["id"])
                if i < len(field):
                    if (
                        j >= 1
                        and (c := number_cells[i + 1][j - 1])["value"]
                        and not number_cells[i + 1][j - 1]["id"] in checked_ids
                    ):
                        values.append(c["value"])
                        checked_ids.append(c["id"])
                    if (c := number_cells[i + 1][j])["value"] and not number_cells[
                        i + 1
                    ][j]["id"] in checked_ids:
                        values.append(c["value"])
                        checked_ids.append(c["id"])
                    if (
                        j < len(line)
                        and (c := number_cells[i + 1][j + 1])["value"]
                        and not number_cells[i + 1][j + 1]["id"] in checked_ids
                    ):
                        values.append(c["value"])
                        checked_ids.append(c["id"])
                if (
                    j >= 1
                    and (c := number_cells[i][j - 1])["value"]
                    and not number_cells[i][j - 1]["id"] in checked_ids
                ):
                    values.append(c["value"])
                    checked_ids.append(c["id"])
                if (
                    j < len(line)
                    and (c := number_cells[i][j + 1])["value"]
                    and not number_cells[i][j + 1]["id"] in checked_ids
                ):
                    values.append(c["value"])
                    checked_ids.append(c["id"])

                if len(values) == 2:
                    gear_sum += values[0] * values[1]

    # print(checked_ids)
    # print(
    #     "\n".join(
    #         [
    #             " ".join(l)
    #             for l in [
    #                 [
    #                     f"{str(cell['value']):>3} [{str(cell['id']):>4}] |"
    #                     for cell in line
    #                 ]
    #                 for line in number_cells
    #             ]
    #         ]
    #     )
    # )
    return gear_sum


print("Part 1:", part_1(input))
print("Part 2:", part_2(input))
