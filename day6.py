from inputs import day_6 as input
import math


def generate_races(input: str):
    return [*zip(*[[int(n) for n in l.split()[1:]] for l in input.splitlines()])]


def generate_race(input: str):
    return [int("".join(l.split()[1:])) for l in input.splitlines()]


def calc_possible(time: int, dist: int):
    possible = 0
    for speed in range(time):
        if dist < speed * (time - speed):
            possible += 1
    return possible


def part_1(input: str):
    return math.prod([calc_possible(*race) for race in generate_races(input)])


def part_2(input: str):
    return calc_possible(*generate_race(input))


print("Part 1:", part_1(input))
print("Part 2:", part_2(input))
