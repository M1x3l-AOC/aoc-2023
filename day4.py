from inputs import day_4 as input


def generate_card_information(input) -> "list[tuple[str,set,set]]":
    infos = []
    for line in input.splitlines():
        [card, numbers] = line.split(":")
        [winning_numbers, my_numbers] = [
            set([int(n) for n in part.strip().replace("  ", " ").split(" ")])
            for part in numbers.split(" | ")
        ]
        infos.append(
            (
                int(card.replace("  ", " ").replace("  ", " ").split(" ")[1]),
                winning_numbers,
                my_numbers,
            )
        )
    return infos


def matching(s1, s2):
    return len(s1 & s2)


def part_1(input):
    num_sum = 0
    for card, winning_numbers, my_numbers in generate_card_information(input):
        if m := matching(winning_numbers, my_numbers):
            num_sum += 2 ** (m - 1)
    return num_sum


def part_2(input):
    amounts = list([1 for _ in input.splitlines()])
    for card, winning_number, my_numbers in generate_card_information(input):
        for i in range(card, card + matching(winning_number, my_numbers)):
            amounts[i] += amounts[card - 1]
    return sum(amounts)


print("Part 1:", part_1(input))
print("Part 2:", part_2(input))
